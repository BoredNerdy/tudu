import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:sizer/sizer.dart';
import 'package:tudu/widget/carouselSlide.dart';

class LandingPage extends StatefulWidget {
  const LandingPage({
    Key? key,
  }) : super(key: key);

  @override
  State<LandingPage> createState() => _LandingPageState();
}

class _LandingPageState extends State<LandingPage> {
  CarouselController carouselController = CarouselController();
  int activePage=0;
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        // Text('TuDu', style: Theme.of(context).textTheme.bodyText1?.copyWith( fontSize: 24)),
        // Text('TuDu', style: Theme.of(context).textTheme.bodyText1?.copyWith(fontFamily: "Roboto", fontSize: 24)),
        Padding(
          padding: EdgeInsets.only(top:16,right: 16),
          child: Align(
            alignment: Alignment.centerRight,
            child: Text(
              'TuDu',
              style: GoogleFonts.montserrat().copyWith(
                fontSize: 24.sp,
                fontWeight: FontWeight.bold,
                color: const Color.fromRGBO(223, 120, 97, 1),
              ),
            ),
          ),
        ),
        CarouselSlider(
          options: CarouselOptions(height: 60.h,
            initialPage: activePage,
            viewportFraction: 1,autoPlay: true),
          items: List.generate(3,(index) {
            return carouselSlide((val){
              carouselController.jumpToPage(val);
              if(val==3) {
                carouselController.stopAutoPlay();
              }
            },index,context);
          }),
          carouselController: carouselController,
        )
      ],
    );
  }
}
