import 'package:flutter/material.dart';
import 'package:sizer/sizer.dart';
import 'package:tudu/landing.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Sizer(builder: (context, orientation, deviceType) {
        return MaterialApp(
          title: 'TuDu',
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            primarySwatch: Colors.blue,
          ),
          home: const Scaffold(
          appBar: null,
          backgroundColor:Color.fromRGBO(252, 248, 232,1),
        body: SafeArea(child: LandingPage()),
          )
        );
      }),
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
    );
  }
}


