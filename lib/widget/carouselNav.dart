import 'package:flutter/material.dart';

Widget carouselNav(ValueSetter<int> changedPage, {int selected = 0}) {
  return Padding(
    padding: const EdgeInsets.only(top:24.0),
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: List<Widget>.generate(3, (index) {
        return GestureDetector(
          onTap: (){
            print("nav : $index");
            changedPage(index);
          },
          child: Container(
            width: 40,
            height: 40,
            decoration: BoxDecoration(
                color: selected == (index) ? const Color.fromRGBO(223, 120, 97, 1) : Colors.white,
                borderRadius: const BorderRadius.all(
                  Radius.circular(20),
                ),
                boxShadow: [ const BoxShadow(color: Colors.black38, blurRadius: 5, offset:  const Offset(0, 0))]),
          ),
        );
      }),
    ),
  );
}
