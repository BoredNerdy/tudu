import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:sizer/sizer.dart';
import 'package:tudu/const/sliderList.dart';
import 'package:tudu/login.dart';

import 'carouselNav.dart';

Widget carouselSlide(ValueSetter<int> changedPage, int selected,BuildContext context) {
  return Container(
    height: 60.h,
    width: 100.w,
    margin: const EdgeInsets.only(left: 16, bottom: 16, top: 5, right: 16),
    padding: const EdgeInsets.all(16),
    decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(16)),
        boxShadow: [BoxShadow(color: Colors.black38, blurRadius: 5, offset: Offset(0, 0))]),
    child: Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        SvgPicture.asset("assets/images/${pictures[selected]}"),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: 16.0),
          child: Text(
            titles[selected],
            style: GoogleFonts.montserrat().copyWith(fontWeight: FontWeight.bold, fontSize: 14.sp),
          ),
        ),
        Expanded(
          child: Text(
            comments[selected],
            style: GoogleFonts.roboto().copyWith(fontSize: 12.sp),
          ),
        ),
      selected == 2 ?  Padding(
          padding: EdgeInsets.all(16),
          child: GestureDetector(
            onTap: () {
              Navigator.pushAndRemoveUntil(context,MaterialPageRoute(builder: (context) =>
                  LoginPage()), (Route<dynamic> route) => false);
            },
            child: Center(
              child: Container(
                padding: EdgeInsets.all(8),
                decoration: BoxDecoration(
                  color: Colors.redAccent,
                  borderRadius: BorderRadius.all(Radius.circular(4)),
                ),
                child: Text('Kayıt Ol', style: GoogleFonts.roboto().copyWith(fontSize: 12.sp, fontWeight: FontWeight.bold, color: Colors.white)),
              ),
            ),
          ),
        ):Container(),
        carouselNav((val) {
          print("slide : $val");
          changedPage(val);
        }, selected: selected),
      ],
    ),
  );
}
