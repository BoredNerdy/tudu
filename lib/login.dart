import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:sizer/sizer.dart';
import 'package:tudu/widget/carouselSlide.dart';

import 'register.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({
    Key? key,
  }) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController controllerUser = TextEditingController();
  TextEditingController controllerPassword = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Sizer(builder: (context, orientation, deviceType) {
      return MaterialApp(
          title: 'TuDu',
          debugShowCheckedModeBanner: false,
          theme: ThemeData(
            primarySwatch: Colors.blue,
          ),
          home: Scaffold(
            appBar: null,
            backgroundColor: const Color.fromRGBO(252, 248, 232, 1),
            body: SafeArea(
              child: Center(
                child: Container(
                  decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(16)),
                      boxShadow: [BoxShadow(color: Colors.black38, blurRadius: 5, offset: Offset(0, 0))]),
                  height: 60.h,
                  width: 100.w,
                  margin: const EdgeInsets.only(left: 16, bottom: 16, top: 5, right: 16),
                  padding: const EdgeInsets.all(16),
                  child: ListView(
                    children: [
                      Padding(
                        padding:  EdgeInsets.only(top:5.h),
                        child: Icon(
                          Icons.check_circle,
                          size: 64.sp,
                          color: Colors.green,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top:8.0),
                        child: Text(
                          'TuDu',
                          style: GoogleFonts.montserrat().copyWith(
                            fontWeight: FontWeight.bold,
                            fontSize: 24.sp,
                            color: const Color.fromRGBO(223, 120, 97, 1),
                          ),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8.0,horizontal: 16),
                        child: TextField(
                          controller: controllerUser,
                          decoration: const InputDecoration(
                            hintText: "Kullanıcı Adını Giriniz",
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.all(Radius.circular(4)),
                            ),
                            prefixIcon: Icon(Icons.account_circle),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 8.0,horizontal: 16),

                        child: TextField(
                          controller: controllerPassword,
                          decoration: const InputDecoration(
                            hintText: "Şifrenizi Giriniz",
                            border: OutlineInputBorder(
                              borderRadius: BorderRadius.all(Radius.circular(4)),
                            ),
                            prefixIcon: Icon(Icons.password),
                          ),
                          obscureText: true,
                        ),
                      ),
                      /*Padding(
                        padding: const EdgeInsets.only(top:8.0),
                        child: IntrinsicWidth(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              GestureDetector(
                                child: Container(
                                  decoration: BoxDecoration(color: Color.fromRGBO(223, 120, 97, 1), borderRadius: BorderRadius.all(Radius.circular(4))),
                                  padding: EdgeInsets.symmetric(vertical: 16, horizontal: 24),
                                  margin: EdgeInsets.symmetric(vertical: 8),
                                  child: Text(
                                    'Giriş Yap',
                                    style: GoogleFonts.roboto().copyWith(color: Colors.white),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                                onTap: () {},
                              ),
                              GestureDetector(
                                child: Container(
                                  decoration:
                                      BoxDecoration(color: Color.fromRGBO(236, 179, 144, 1), borderRadius: BorderRadius.all(Radius.circular(4))),
                                  padding: EdgeInsets.symmetric(vertical: 16, horizontal: 24),
                                  margin: EdgeInsets.symmetric(vertical: 4),
                                  child: Text(
                                    'Kayıt Ol',
                                    style: GoogleFonts.roboto().copyWith(color: Colors.white),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                                onTap: () {},
                              ),
                            ],
                          ),
                        ),
                      ),*/
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: GestureDetector(
                          child: Container(
                            width:30,
                            decoration: BoxDecoration(color: Color.fromRGBO(223, 120, 97, 1), borderRadius: BorderRadius.all(Radius.circular(4))),
                            padding: EdgeInsets.symmetric(vertical: 16, horizontal: 24),
                            margin: EdgeInsets.symmetric(vertical: 8),
                            child: Text(
                              'Giriş Yap',
                              style: GoogleFonts.roboto().copyWith(color: Colors.white),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          onTap: () {},
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: GestureDetector(
                          child: Container(
                            width:75,
                            decoration:
                            BoxDecoration(color: Color.fromRGBO(236, 179, 144, 1), borderRadius: BorderRadius.all(Radius.circular(4))),
                            padding: EdgeInsets.symmetric(vertical: 16, horizontal: 24),
                            margin: EdgeInsets.symmetric(vertical: 4),
                            child: Text(
                              'Kayıt Ol',
                              style: GoogleFonts.roboto().copyWith(color: Colors.white),
                              textAlign: TextAlign.center,
                            ),
                          ),
                          onTap: () {
                            Navigator.push(context,MaterialPageRoute(builder: (context) =>
                                RegisterPage()),);
                          },
                        ),
                      ),
                      TextButton(
                        onPressed: () {},
                        child: Text('Şifremi Unuttum',
                            style: GoogleFonts.roboto().copyWith(
                              color: Color.fromRGBO(236, 179, 144, 1),
                            )),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ));
    });
  }
}
